const VideoTutorialContribution = require('./models').VideoTutorialContribution;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
        VideoTutorialContribution.count(req.query)
            .then(count => res.json(count))
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message)
            })

    })


    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest } = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = VideoTutorialContribution.findOne(rest);
        } else {
            q = VideoTutorialContribution.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((videoTutorialContributions) => {
            return res.json(videoTutorialContributions);
        })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.post('/', (req, res) => {
        const data = req.body;
        VideoTutorialContribution.create(data)
            .then((videoTutorialContribution) => {
                return res.json(videoTutorialContribution);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        VideoTutorialContribution.update(conditions, { $set: values }, { ...options, multi: true })
            .then(() => VideoTutorialContribution.find(conditions))
            .then(videoTutorialContributions => {
                return res.json(videoTutorialContributions);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/', (req, res) => {
        let conditions = req.body;
        let videoTutorialContributions;
        VideoTutorialContribution.find(conditions)
            .then((a) => {
                videoTutorialContributions = a;
                return VideoTutorialContribution.remove(conditions)
            })
            .then(() => {
                return res.json(videoTutorialContributions);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.get('/:id', (req, res) => {
        VideoTutorialContribution.findById(req.params.id)
            .then((videoTutorialContribution) => {
                return res.json(videoTutorialContribution);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        VideoTutorialContribution.findByIdAndUpdate(id, { $set: changes })
            .then(() => VideoTutorialContribution.findById(id))
            .then(videoTutorialContribution => {
                return res.json(videoTutorialContribution);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    router.delete('/:id', (req, res) => {
        const { id } = req.params;
        let deletedVideoTutorialContribution;
        VideoTutorialContribution.findById(id)
            .then(videoTutorialContribution => {
                deletedVideoTutorialContribution = videoTutorialContribution;
                return VideoTutorialContribution.findByIdAndRemove(id)
            })
            .then(() => {
                return res.json(deletedVideoTutorialContribution);
            })
            .catch(err => {
                console.log(err);
                return res.status(400).send(err.message);
            })
    })

    return router;
}