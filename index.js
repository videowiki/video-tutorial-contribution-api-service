const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');
const videowikiGenerators = require('@videowiki/generators');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
const upload = multer({ storage: storage })

const mongoose = require('mongoose');
const DB_CONNECTION = process.env.VIDEO_TUTORIAL_CONTRIBUTION_SERVICE_DATABASE_URL;
let mongoConnection;
mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection });
    
    app.use('/db', require('./dbRoutes')(createRouter()))

    app.all('*', (req, res, next) => {
        if (req.headers['vw-user-data']) {
            try {
                const user = JSON.parse(req.headers['vw-user-data']);
                req.user = user;
            } catch (e) {
                console.log(e);
            }
        }
        next();
    })

    app.post('/', upload.any(), controller.uploadVideo)
    app.get('/', controller.getVideos)


})
.catch(err => {
    console.log('mongo connection error', err);
    process.exit(1);
})



const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
